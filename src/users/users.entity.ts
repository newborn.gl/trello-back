import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { isEmail } from 'class-validator';

const entity = 'users';

@Entity({ name: entity })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;
}
