FROM node:14.16-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --production

COPY . .

RUN yarn run build && yarn run cp-env

COPY ./tsconfig.json ./

RUN apk add bash
ENTRYPOINT ["sh", "-c"]

CMD ["node -r ./tsconfig-paths-bootstrap.js dist/src/main.js"]